/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class testWriteplayer {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Player x = new Player('X');
            Player o = new Player('O');
            x.win();
            o.win();
            x.loss();
            o.loss();
            x.draw();
            o.draw();
            System.out.println(x);
            System.out.println(o);
            
            File file = new File("players.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testWriteplayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testWriteplayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(testWriteplayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class testReadPlayer {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            File file = new File("players.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Player o = (Player) ois.readObject();
            Player x = (Player) ois.readObject();
            ois.close();
            fis.close();
            System.out.println(o);
            System.out.println(x);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(testReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(testReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;


/**
 *
 * @author HP
 */
public class Friend implements Serializable{
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId = 1;
    
    public Friend(String name,int age, String tel){
        this.name = name;
        this.age = age;
        this.tel = tel;
 
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age<0){
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Age is lower than 0!!");
            }
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}

